const {Game} = require('../classes/game.js');
const {Analyse} = require('../classes/analyse.js');


if (process.argv[2]==='play') {
    const g= new Game;
    g.startGame();
}
else if (process.argv[2]==='analyse'){
    const a=new Analyse();
    a.logStats();
}