const {Board} = require('./board.js');
const {User} = require('./user.js');
const constants = require('../constants/constants.js');
const readline = require('readline');
const fs = require('fs');
const path = require('path');


const rl = readline.createInterface({
    output: process.stdout,
    input: process.stdin
});


class Game{
    constructor(){
        this.board=new Board;
    }

    async step(){
        try {
            const coordinates = await this.askCoordinates(this.currentPlayer);
            this.board.setCell(coordinates[0], coordinates[1], this.currentPlayer.sign);
            console.clear();
            this.board.logMatrix();
            if (this.board.checkWinner()){
                this.endGame(this.board.checkWinner());
                return;
            }
            if (!this.board.emptyCellsLeft()){
                this.endGame();
                return;
            }
            this.changeCurrentPlayer();
            this.step();
            
        } catch (error) {
            console.clear();
            this.board.logMatrix();
            console.log(error);
            this.step();
        }
    }

    askCoordinates(currentPlayer) {
        return new Promise((resolve, reject) => {
            rl.question(`Hey, ${currentPlayer.name}(${currentPlayer.sign}), enter coordinates separated by comma:\n`, (data)=>{
                if (data) {
                    const coordinates=data.split(',');
                    if (coordinates.length===2) {
                        resolve(coordinates);
                        
                    }
                    else{
                        reject('\x1b[31m Please enter data correctly! \x1b[0m');
                    }
                }
                else{
                    reject('\x1b[31m Please enter something! \x1b[0m');
                }
            });
        });
    } 

    askName(number) {
        return new Promise((resolve, reject) => {
            rl.question(`Hello, player #${number}, enter your name:\n`, (data)=>{
                if (data) {
                    resolve(data);
                }
                else{
                    reject('\x1b[31m Please enter something! \x1b[0m');
                }
            });
        });
    } 

    async askNames(){
        try {
            const name1 = await this.askName(1);
            const name2 = await this.askName(2);
            if (name1===name2) {
                throw '\x1b[31m Names should be different! \x1b[0m';
            }
            else{
                this.player1=new User(name1, constants.X);
                this.player2=new User(name2, constants.O);
                this.playGame();
            }
        } catch (error) {
            console.clear();
            console.log(error);
            this.askNames();
        }
    }

    getFreeCells(){
        this.board.emptyCellsLeft();
    }

    playGame(){
        console.clear();
        this.currentPlayer=this.player1;
        this.board.logMatrix();
        this.step();
    }

    async startGame(){
        console.clear();
        this.askNames();
    }

    async recordStats(winner=false, loser=false){
        const date = Date.now();
        if (winner && loser) {
            await fs.promises.appendFile(
                path.join(__dirname, '../dist/statistics.csv'), `\n${winner.name},win,${date}`,
                () => {}
            );
            await fs.promises.appendFile(
                path.join(__dirname, '../dist/statistics.csv'), `\n${loser.name},lose,${date}`,
                () => {}
            );
        }
        else{
            await fs.promises.appendFile(
                path.join(__dirname, '../dist/statistics.csv'), `\n${this.player1.name},draw,${date}`,
                () => {}
            );
            await fs.promises.appendFile(
                path.join(__dirname, '../dist/statistics.csv'), `\n${this.player2.name},draw,${date}`,
                () => {}
            );
        }
    }

    getWinnerBySign(winnerSign){
        if (this.player1.sign===winnerSign) {
            return this.player1;
        }
        else if (this.player2.sign===winnerSign) {
            return this.player2;
        }
    }

    getLoserBySign(winnerSign){
        if (this.player1.sign===winnerSign) {
            return this.player2;
        }
        else if (this.player2.sign===winnerSign) {
            return this.player1;
        }
    }
    
    endGame(winnerSign=false){
        rl.close();
        if (winnerSign) {
            const winner = this.getWinnerBySign(winnerSign);
            const loser = this.getLoserBySign(winnerSign);
            this.recordStats(winner, loser);
            console.log(`\x1b[32m Winner is ${winner.name}! \x1b[0m`)
        }
        else{
            console.log('\x1b[32m No free cells left! \x1b[0m')
        }
    }

    changeCurrentPlayer(){
        if (this.currentPlayer.name===this.player1.name) {
            this.currentPlayer=this.player2;
        }
        else{
            this.currentPlayer=this.player1;
        }
    }
}


module.exports ={
    Game
}