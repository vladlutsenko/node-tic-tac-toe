const {Cell} = require('./cell.js');

class Board{
    constructor(){
        this._matrix=[[],[],[]];
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                this._matrix[i][j] = new Cell;
            }
        }
    }

    get matrix() {
        return this._matrix;
    }

    getCell(x, y){
        return this._matrix[x][y].value;
    }

    emptyCellsLeft(){
        var answer=false;
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                if (this._matrix[i][j].empty){
                    answer=true;
                };
            }
        }
        return answer;
    }

    setCell(x, y, val){

        if ((x.match(/^\d$/)) && (y.match(/^\d$/)) && (x<=3 && x>=0) && (y<=3 && y>=0)) {
            this._matrix[x][y].value=val;
            
        }
        else{
            throw '\x1b[31m Please enter correct coordinates! \x1b[0m';
        }
    }

    logMatrix(){
        for (let i = 0; i < 3; i++) {
            console.log(`|${this._matrix[0][i].value}|${this._matrix[1][i].value}|${this._matrix[2][i].value}|`);
            console.log('--------');
        }
    }

    checkWinner(){
        if (this.checkDiagonal()) {
            return this.checkDiagonal();
        }
        for (let i = 0; i < 3; i++) {
            if (this.checkRow(i)) {
                return this.checkRow(i);
            }
            if (this.checkColumn(i)) {
                return this.checkColumn(i);
            }
        }
        return false;
    }

    checkRow(row){
        if (this.matrix[0][row].value===this.matrix[1][row].value && this.matrix[1][row].value===this.matrix[2][row].value && this.matrix[0][row].value!==' ') {
            return this.matrix[0][row].value;
        }
        else{
            return false;
        }
    }

    checkColumn(col){
        if (this.matrix[col][0].value===this.matrix[col][1].value && this.matrix[col][1].value===this.matrix[col][2].value && this.matrix[col][0].value!==' ') {
            return this.matrix[col][0].value;
        }
        else{
            return false;
        }
    }

    checkDiagonal(){
        if (this.matrix[0][0].value===this.matrix[1][1].value && this.matrix[1][1].value===this.matrix[2][2].value && this.matrix[1][1].value!==' ') {
            return this.matrix[0][0].value;
        }
        else if(this.matrix[2][0].value===this.matrix[1][1].value && this.matrix[1][1].value===this.matrix[0][2].value && this.matrix[1][1].value!==' '){
            return this.matrix[2][0].value;
        }
        else{
            return false;
        }
    }
}


module.exports ={
    Board
}
