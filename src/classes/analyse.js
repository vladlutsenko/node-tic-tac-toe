const readline = require('readline');
const path = require('path');
const fs = require('fs');


class Analyse{
    constructor(){
        this.path=path.join(__dirname, '../dist/statistics.csv');
    }

    readStats(){
        return new Promise((resolve, reject) => {
            if (fs.existsSync(this.path)) {
                var data=[];
                const rl = readline.createInterface({
                    input: fs.createReadStream(this.path)
                })
                .on('line', (line)=>{
                    if (line) {
                        var row = line.split(',');
                        data.push(row);
                    }
                })
                .on('close', () => {
                    rl.close();
                    resolve(data);
                });
            }
            else{
                reject('no statistics file');
            }
        
        })
    }

    async analyseStats(){
        this.data=await this.readStats();
        var processedData=[];
        this.data.forEach(record => {
            var playerExists=false;
            processedData.forEach(player => {
                if (player.name===record[0]){
                    playerExists=true;
                    if (record[1]==='win') {
                        player.wins++;
                    }
                    else if (record[1]==='lose') {
                        player.loses++;
                    }
                    else if (record[1]==='draw'){
                        player.draws++;
                    }
                }
            });
            if (!playerExists) {
                const player={};
                player.name=record[0];
                player.wins=0;
                player.loses=0;
                player.draws=0;
                if (record[1]==='win') {
                    player.wins++;
                }
                else if (record[1]==='lose') {
                    player.loses++;
                }
                else if (record[1]==='draw'){
                    player.draws++;
                }
                processedData.push(player);
            }
        });
        return processedData;
    }

    async logStats(){
        const stats= await this.analyseStats();
        console.clear();
        stats.forEach(player => {
            console.log(`${player.name}:\n Wins: ${player.wins}, Loses: ${player.loses}, Draws: ${player.draws}\n\n`);
        });
    }
        

}


module.exports ={
    Analyse
}