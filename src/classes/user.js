class User{
    constructor(name, sign){
        this.name=name;
        this.sign=sign;
    }

    get name(){
        return this._name;
    }

    get sign(){
        return this._sign;
    }

    set name(name){
        this._name=name;
    }

    set sign(sign){
        this._sign=sign;
    }
}


module.exports ={
    User
}