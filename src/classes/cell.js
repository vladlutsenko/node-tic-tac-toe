class Cell{
    constructor(){
        this._value=' ';
        this.isEmpty=true;
    }

    get value() {
        return this._value;
    }

    get empty(){
        return this.isEmpty;
    }
    
    set value(value) {
        if (this.isEmpty) {
            this._value = value;
            this.isEmpty=false;
        }
        else{
            throw '\x1b[31m This cell is already used! \x1b[0m';
        }
    }
}


module.exports ={
    Cell
}