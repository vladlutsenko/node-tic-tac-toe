# Tic Tac Toe

To start the game enter command:
```
node src/game/index.js play
```

To watch statistics enter command:
```
node src/game/index.js analyse
```